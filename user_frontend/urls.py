from django.urls import path
from . import views

app_name = 'user'
urlpatterns = [
    path('', views.home, name='home'),
    path('signup', views.signup, name='user-signup'),
    path('login', views.login, name='user-login'),
    path('user/update-password', views.update_password, name='update-password'),
    path('user/logout', views.logout, name='user-logout'),

    path('user/dashboard', views.dashboard, name='dashboard'),
    path('user/service-request', views.raise_service_request,
         name="service-request"),
    path('user/register-device', views.register_device,
         name="register-device"),
    path('user/request-status', views.request_status,
         name="request-status"),
    path('contact', views.contact_us, name="contact-us"),
    path('user/devices', views.user_registered_devices,
         name="registered-devices"),
    path('user/requested-services', views.user_requested_services,
         name="requested-services"),
]
