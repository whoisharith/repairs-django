import json
import requests
from django.contrib.auth.hashers import make_password, check_password
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from rest_framework.authtoken.models import Token

from api_m.models import User
from repairs import settings


def home(request):
    return render(request, 'home/home.html')


def dashboard(request):
    return render(request, 'user_dashboard/dashboard.html')


def signup(request):
    if request.method == 'POST':
        url = request.build_absolute_uri('api-m/register/')
        data = {
            'first_name': request.POST['first_name'],
            'last_name': request.POST['last_name'],
            'email': request.POST['email'],
            'phone': request.POST['phno'],
            'password': request.POST['password1'],
            'password2': request.POST['password2']
        }
        data = json.dumps(data)
        headers = {
            'Content-Type': 'application/json'
        }
        response = requests.post(url=url, data=data, headers=headers)
        if response.status_code == 409:
            msg = 'Email Id should be unique'
            return render(request, 'home/signup.html', {'error_msg': msg})
        if response.status_code == 201:
            return redirect('/login')
    return render(request, 'home/signup.html')


def login(request):
    if request.method == 'POST':
        url = request.build_absolute_uri('api-m/login/')
        data = {
            'email': request.POST['email'],
            'password': request.POST['password']
        }
        data = json.dumps(data)
        headers = {
            'Content-Type': 'application/json'
        }
        response = requests.post(url=url, data=data, headers=headers)
        if response.status_code == 200:
            token = json.loads(response.content)['token']
            is_manager = json.loads(response.content)['is_manager']
            if is_manager:
                response = HttpResponseRedirect(reverse('manager:home-page'))
            else:
                response = HttpResponseRedirect(reverse('user:dashboard'))
            response.set_cookie('Token', value=token)
            request.session['email'] = request.POST['email']
            return response
        if response.status_code == 401:
            msg = 'Password or Email Id are not Valid'
            return render(request, 'home/login.html', {'error_msg': msg})
    return render(request, 'home/login.html')


def register_device(request):
    if request.method == 'POST':
        url = settings.USER_API_DOMAIN + 'devices/'
        data = {
            'manufacturer': request.POST['manufacturer'],
            'model': request.POST['model'],
            'serial_number': request.POST['serial_number'],
            'type_of_device': request.POST['type_of_device']
        }
        data = json.dumps(data)
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Token ' + request.COOKIES['Token']
        }
        response = requests.post(url=url, data=data, headers=headers)
        if response.status_code == 201:
            return HttpResponseRedirect(reverse('user:service-request'))
    return render(request, 'user_dashboard/register_device.html')


def raise_service_request(request):
    url = settings.USER_API_DOMAIN + 'devices/'
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + request.COOKIES['Token']
    }
    response = requests.get(url=url, headers=headers)
    device_list = []
    for device_detail in json.loads(response.text):
        id = device_detail['id']
        model = device_detail['model']
        serial_number = device_detail['serial_number']
        device_list.append({'id': id, 'model_and_serial_number': model + \
                                                                 " " + serial_number})
    msg = ''
    if request.method == 'POST':
        url = settings.USER_API_DOMAIN + 'requests/'
        data = {
            'device': request.POST['device'],
            'description': request.POST['descriptions'],
        }
        data = json.dumps(data)
        response = requests.post(url=url, data=data, headers=headers)
        if len(request.POST['descriptions']) == 0:
            msg = 'Please Enter device descriptions '
        elif response.status_code == 201:
            return HttpResponseRedirect(reverse('user:dashboard'))
        else:
            msg = 'Sorry!!! You can not register new service request unless and until registered service request gets ' \
                  'resolved '
    return render(request, 'user_dashboard/service_request.html',
                  {'device_list': device_list, 'error_msg': msg})


def request_status(request):
    url = settings.USER_API_DOMAIN + 'devices/'
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + request.COOKIES['Token']
    }
    response = requests.get(url=url, headers=headers)
    device_list = []
    msg = ''
    for device_detail in json.loads(response.text):
        id = device_detail['id']
        model = device_detail['model']
        serial_number = device_detail['serial_number']
        request_list = device_detail['requests']
        device_list.append(
            {'id': id, 'model_and_serial_number': model + " " + serial_number,
             'request_list': request_list}
        )
    if request.method == 'POST':
        device_id = request.POST['device']
        if len(request.POST['request']) != 0:
            request_id = int(request.POST['request'])
            json_data = json.loads(response.text)
            for device_data in json_data:
                if device_data['id'] == int(device_id):
                    device_detail = device_data
                    request_status = {
                        'Device Type': device_detail['type_of_device']['name'],
                        'Model': device_detail['model'],
                        'Manufacturer': device_detail['manufacturer'],
                        'Serial Number': device_detail['serial_number'],
                        'Owner Email Id': device_detail['owner']['email']
                    }
                    for request_detail in device_detail['requests']:
                        if request_detail['id'] == request_id:
                            index = \
                                device_detail['requests'].index(request_detail)
                            request_status['Request Id'] = \
                                device_detail['requests'][index]['id']
                            request_status['Description'] = \
                                device_detail['requests'][index]['description']
                            request_status['Status'] = \
                                device_detail['requests'][index]['stage_status']
                            return render(request, 'user_dashboard/status.html',
                                          {'request_status': request_status})
        msg = 'Please Select Service Request..'
    return render(request, 'user_dashboard/request_status.html', \
                  {'device_list': device_list, 'error_msg': msg})


def contact_us(request):
    return render(request, 'home/contact.html')


def user_registered_devices(request):
    url = settings.USER_API_DOMAIN + 'devices/'
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + request.COOKIES['Token']
    }
    response = requests.get(url=url, headers=headers)
    devices = []
    for device_detail in json.loads(response.text):
        devices.append({'Id': device_detail['id'],
                        'Manufacturer': device_detail['manufacturer'],
                        'Model': device_detail['model'],
                        'Serial Number': device_detail['serial_number']
                        })
    if len(devices) == 0:
        msg = 'You have not registered any devices.'
        return render(request, 'user_dashboard/error.html', {'error_msg': msg})
    else:
        return render(request, 'user_dashboard/registered_devices.html',
                      {'devices': devices})


def user_requested_services(request):
    url = settings.USER_API_DOMAIN + 'requests/'
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + request.COOKIES['Token']
    }
    response = requests.get(url=url, headers=headers)
    devices = []
    for device_detail in json.loads(response.text):
        devices.append({
            'Device Id': device_detail['device']['id'],
            'Manufacturer': device_detail['device']['manufacturer'],
            'Model': device_detail['device']['model'],
            'Serial Number': device_detail['device']['serial_number'],
            'Description': device_detail['description']
        })
    if len(devices) == 0:
        msg = 'You have not requested for any services.'
        return render(request, 'user_dashboard/error.html', {'error_msg': msg})
    else:
        return render(request, 'user_dashboard/requested_services.html',
                      {'devices': devices})


def update_password(request):
    if request.method == 'POST':
        current_password = request.POST['password1']
        new_password = request.POST['password2']
        email = Token.objects.get(key=request.COOKIES['Token']).user
        if new_password == current_password:
            msg = 'New password should be different from current password'
            return render(request, 'user_dashboard/update_password.html', \
                          {'error_msg': msg})
        else:
            user = User.objects.get(email__exact=email)
            if check_password(current_password, user.password):
                user.set_password(new_password)
                user.save()
                return HttpResponseRedirect(reverse('user:dashboard'))
            else:
                msg = 'Please Enter Correct Current Password'
                return render(request, 'user_dashboard/update_password.html', \
                              {'error_msg': msg})
    return render(request, 'user_dashboard/update_password.html')


def logout(request):
    url = settings.ADMIN_API_DOMAIN + 'logout/'
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + request.COOKIES['Token']
    }
    response = requests.post(url=url, headers=headers)
    if response.status_code == 200:
        response = HttpResponseRedirect(reverse('user:home'))
        response.delete_cookie('Token')
        del request.session['email']
        return response
