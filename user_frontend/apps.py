from django.apps import AppConfig


class UserFrontendConfig(AppConfig):
    name = 'user_frontend'
