import simplecrypt
import base64

def decrypt_and_decode_string(encrypted, salt):
    """Take an encrypted string as a parameter and decrypt it using
    simplecrypt and a decryption key.\n
    :param encrypted : string, encrypted password.\n
    :param salt : string, decryption key for simplecrypt.\n
    :return decrypted: string, password in plaintext."""
    try:
        decrypted = simplecrypt.decrypt(salt,\
            base64.b64decode(encrypted)).decode('utf-8')
        return decrypted
    except simplecrypt.DecryptionException as wrong_salt:
        return wrong_salt
