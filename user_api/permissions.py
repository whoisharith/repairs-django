from rest_framework import permissions


class DeviceOwnerOnly(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        return request.user == obj.owner


class RequestOwnerOnly(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        return request.user == obj.device.owner
