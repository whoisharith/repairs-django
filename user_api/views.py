from rest_framework import generics, status
from rest_framework.decorators import permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from api_m import models, serializers
from user_api import permissions as custom_permissions


class DeviceList(generics.ListCreateAPIView):
    """Class which defines the view for device API."""
    serializer_class = serializers.DeviceSerializer

    def get_queryset(self):
        return self.request.user.devices.all()


class ServiceRequestList(generics.ListCreateAPIView):
    """Class which defines the view for service request API."""
    serializer_class = serializers.RequestSerializer

    def get_queryset(self):
        return models.Request.objects.filter(
            device__in=self.request.user.devices.all()
        )

    def post(self, request, *args, **kwargs):
        try:
            device = request.user.devices.get(pk=request.data['device'])
            active_request_exists = device.requests.all() and\
                device.requests.last().stage != models.Stage.objects.last()
            if active_request_exists:
                return Response(
                    {"error": "Request already active for this device."},
                    status=status.HTTP_400_BAD_REQUEST
                )
        except models.Device.DoesNotExist:
            return Response(
                {"error": "No such device"}, status=status.HTTP_404_NOT_FOUND
            )
        except KeyError:
            return Response(
                {"error": "Bad request"}, status=status.HTTP_400_BAD_REQUEST
            )

        return super(ServiceRequestList, self).post(request, *args, **kwargs)


class RequestDetail(generics.RetrieveUpdateDestroyAPIView):
    """Class which defines the view for service request detail API."""
    serializer_class = serializers.RequestSerializer
    queryset = models.Request.objects.all()
    permission_classes = [custom_permissions.RequestOwnerOnly]


class DeviceDetail(generics.RetrieveUpdateDestroyAPIView):
    """Class which defines the view for service request detail API."""
    serializer_class = serializers.DeviceSerializer
    queryset = models.Device.objects.all()
    permission_classes = [custom_permissions.DeviceOwnerOnly]


@permission_classes((AllowAny,))
class ServicesList(generics.ListCreateAPIView):
    serializer_class = serializers.TypeSerializer

    def get_queryset(self):
        return models.Type.objects.values('id', 'name')
