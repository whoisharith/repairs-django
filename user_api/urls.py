from django.conf.urls import url
from django.urls import path

from . import views
app_name = 'user_api'
urlpatterns = [
    path('devices/', views.DeviceList.as_view()),
    path('devices/<int:pk>/', views.DeviceDetail.as_view()),
    path('requests/', views.ServiceRequestList.as_view()),
    path('requests/<int:pk>/', views.RequestDetail.as_view()),
    path('services/', views.ServicesList.as_view()),
]
