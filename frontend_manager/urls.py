from django.urls import path

from . import views

app_name = 'frontend_manager'

urlpatterns = [
    path('home', views.ManagerHome.as_view(), name='home-page'),
    path('archive', views.CompletedRequests.as_view(),
         name='requests-completed'),

    path('requests/move', views.MoveStage.as_view(), name='request-move-stage'),
    path('requests/<int:pk>', views.RequestInfo.as_view(), name='request-info'),

    path('update-password/', views.UpdatePassword.as_view(),
         name='update-password'),

    path('logout/', views.logout_user, name='logout'),
]
