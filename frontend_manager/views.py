import json
import requests

from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic, View

from . utils import get_current_user, get_stages_list


class ManagerHome(generic.TemplateView):
    template_name = 'manager/index.html'

    def get(self, request, *args, **kwargs):
        user = get_current_user(request)
        if not user:
            return HttpResponseRedirect('/login')
        if not user['is_manager']:
            return HttpResponseRedirect('/user/dashboard')
        return super().get(
            super(ManagerHome, self), request, *args, **kwargs
        )

    def get_context_data(self, *args, **kwargs):
        request_list = []
        url = self.request.build_absolute_uri(
            reverse('api-m:request-list', kwargs={'term': 'pending'})
        )
        headers = {
            'Authorization': f'Token {self.request.COOKIES["Token"]}'
        }
        api_response = requests.get(url=url, headers=headers)
        if api_response.status_code == 200:
            request_list = json.loads(api_response.content)
        return {
            'request_list': request_list,
            'email': self.request.session['email']
        }


class CompletedRequests(generic.TemplateView):
    template_name = 'manager/archive.html'

    def get(self, request, *args, **kwargs):
        user = get_current_user(request)
        if not user:
            return HttpResponseRedirect('/login')
        if not user['is_manager']:
            return HttpResponseRedirect('/user/dashboard')
        return super().get(
            super(CompletedRequests, self), request, *args, **kwargs
        )

    def get_context_data(self, *args, **kwargs):
        request_list = []
        url = self.request.build_absolute_uri(
            reverse('api-m:request-list', kwargs={'term': 'completed'})
        )
        headers = {
            'Authorization': f'Token {self.request.COOKIES["Token"]}'
        }
        api_response = requests.get(url=url, headers=headers)
        if api_response.status_code == 200:
            request_list = json.loads(api_response.content)
        return {
            'request_list': request_list,
            'email': self.request.session['email']
        }


class RequestInfo(generic.TemplateView):
    template_name = 'manager/request_detail.html'

    def get(self, request, *args, **kwargs):
        user = get_current_user(request)
        if not user:
            return HttpResponseRedirect('/login')
        if not user['is_manager']:
            return HttpResponseRedirect('/user/dashboard')
        return super().get(
            super(RequestInfo, self), request, *args, **kwargs
        )

    def get_context_data(self, *args, **kwargs):
        service_request = {}
        url = self.request.build_absolute_uri(
            reverse('api-m:request-detail', kwargs={'pk': kwargs['pk']})
        )
        headers = {
            'Authorization': f'Token {self.request.COOKIES["Token"]}'
        }
        api_response = requests.get(url=url, headers=headers)
        if api_response.status_code == 200:
            service_request = json.loads(api_response.content)
        return {
            'request': service_request,
            'stages': get_stages_list(self.request),
            'email': self.request.session['email']
        }


class MoveStage(View):
    def post(self, request, *args, **kwargs):
        user = get_current_user(request)
        if not user:
            return HttpResponseRedirect('/login')
        if not user['is_manager']:
            return HttpResponseRedirect('/user/dashboard')
        data = json.dumps({
            'request': request.POST['request-choice'],
            'to_stage': request.POST['to-stage'],
            'stage_status': request.POST['stage_status'],
            'handler': get_current_user(request).get('id')
        })
        url = request.build_absolute_uri(reverse('api-m:action-list'))
        headers = {
            'Authorization': f'Token {request.COOKIES["Token"]}',
            'Content-type': 'application/json'
        }
        api_response = requests.post(url=url, headers=headers, data=data)
        if api_response.status_code == 201:
            messages.info(request, message="Request updated.")
        else:
            messages.error(request, message="Something went wrong.")
        return HttpResponseRedirect(
            reverse(
                'manager:request-info',
                kwargs={'pk': request.POST['request-choice']}
            )
        )


class UpdatePassword(generic.TemplateView):
    template_name = 'manager/update_password.html'

    def get(self, request, *args, **kwargs):
        user = get_current_user(request)
        if not user:
            return HttpResponseRedirect('/login')
        if not user['is_manager']:
            return HttpResponseRedirect('/user/dashboard')
        return super().get(
            super(UpdatePassword, self), request, *args, **kwargs
        )

    def get_context_data(self, *args, **kwargs):
        return {
            'email': self.request.session['email']
        }

    def post(self, request, *args, **kwargs):
        user = get_current_user(request)
        if not user:
            return HttpResponseRedirect('/login')
        if not user['is_manager']:
            return HttpResponseRedirect('/user/dashboard')
        data = json.dumps({
            'old_password': request.POST.get('password1', ''),
            'new_password': request.POST.get('password2', '')
        })
        url = request.build_absolute_uri(reverse('api-m:update-password'))
        headers = {
            'Authorization': f'Token {request.COOKIES["Token"]}',
            'Content-type': 'application/json'
        }
        api_response = requests.post(url=url, headers=headers, data=data)
        messages.info(request,
                      message=json.loads(api_response.content)['message'])
        if api_response.status_code == 200:
            return HttpResponseRedirect(reverse('manager:home-page'))
        return HttpResponseRedirect(reverse("manager:update-password"))


def logout_user(request):
    headers = {
        'Authorization': f'Token {request.COOKIES["Token"]}'
    }
    url = request.build_absolute_uri(reverse('api-m:logout-user'))
    api_response = requests.post(url=url, headers=headers)
    if api_response.status_code == 200:
        ui_response = HttpResponseRedirect('/')
        ui_response.delete_cookie("Token")
        return ui_response
    return HttpResponseRedirect(reverse('manager:home-page'))
