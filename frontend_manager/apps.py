from django.apps import AppConfig


class FrontendManagerConfig(AppConfig):
    name = 'frontend_manager'
