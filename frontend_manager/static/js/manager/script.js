// radioButtons = document.getElementsByClassName('request-radio-button')
requestTable = document.getElementById('request-table');
textAreaDiv = document.getElementById('action-description-div');
hiddenInput = document.getElementById('stage-input');
nextStageForm = document.getElementById('next-stage-form');

selectedButton = undefined;
if (requestTable){
    requestTable.addEventListener('click', event => {
        selectedButton = document.querySelector('input[name="request-choice"]:checked').value;
        if (selectedButton){
            textAreaDiv.style.display = "flex";
            hiddenInput.style.display = 'none';
        }
        else {
            textAreaDiv.style.display = "none";
        }
    });
}

function moveNextStage(){
    currentStage = document.getElementById(`stage-info-${selectedButton}`).textContent;
    hiddenInput.value = Number(currentStage) + 1;
    if (hiddenInput.value == 0){
        alert("Cannot move back from stage 1.")
    } else if (hiddenInput.value > 6){
        alert("Request already processed. Cannot move forward.")
    } else {
        nextStageForm.submit()
    }
}
function editCurrentStageStatus(){
    currentStage = document.getElementById(`stage-info-${selectedButton}`).textContent;
    hiddenInput.value = Number(currentStage);
    if (hiddenInput.value == 0){
        alert("Cannot move back from stage 1.")
    } else if (hiddenInput.value >= 6){
        alert("Request already processed. Cannot move forward.")
    } else {
        nextStageForm.submit()
    }
}

function moveNextStageDetail(){
    hiddenInput = document.getElementById('stage-input');
    stageDetailForm = document.getElementById('next-stage-form-detail');
    hiddenInput.value = Number(hiddenInput.value) + 1;
    if (hiddenInput.value == 0){
        alert("Cannot move back from stage 1.")
    } else if (hiddenInput.value > 6){
        alert("Request already processed. Cannot move forward.")
    } else {
        stageDetailForm.submit()
    }
}

function editCurrentStageStatusDetail(){
    hiddenInput = document.getElementById('stage-input');
    stageDetailForm = document.getElementById('next-stage-form-detail');
    if (hiddenInput.value == 0){
        alert("Cannot move back from stage 1.")
    } else if (hiddenInput.value >= 6){
        alert("Request already processed. Cannot update.")
    } else {
        stageDetailForm.submit()
    }
}



function showCustomerForm(){
    customerForm = document.getElementById('customer-edit-form');
    customerEditButton = document.getElementById('customer-edit-button');
    if (customerForm.style.visibility == 'hidden'){
        customerForm.style.visibility = 'visible';
        customerEditButton.innerText = 'Cancel';
    } else {
        customerForm.style.visibility = 'hidden';
        customerEditButton.innerText = 'Edit details';
    }
}
