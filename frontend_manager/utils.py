import json
import requests

from django.urls import reverse


def get_current_user(request):
    url = request.build_absolute_uri(reverse('api-m:current-user'))
    try:
        headers = {
            'Authorization': f'Token {request.COOKIES["Token"]}'
        }
        response = requests.get(url=url, headers=headers)
        if response.status_code == 200:
            return json.loads(response.content)
        return {}
    except KeyError:
        return {}


def get_stages_list(request):
    url = request.build_absolute_uri(reverse('api-m:stage-list'))
    try:
        headers = {
            'Authorization': f'Token {request.COOKIES["Token"]}'
        }
        response = requests.get(url=url, headers=headers)
        if response.status_code == 200:
            return json.loads(response.content)
        return {}
    except KeyError:
        return {}
