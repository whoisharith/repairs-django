from rest_framework import permissions


class ManagerOnly(permissions.BasePermission):
    """Custom permission class which allows requests which access to users
    with is_manager = True."""
    def has_permission(self, request, view):
        """Permits the request if the user making the request is a manager."""
        return request.user.is_manager

    def has_object_permission(self, request, view, obj):
        """Permits the request if the user making the request is a manager."""
        return request.user.is_manager
