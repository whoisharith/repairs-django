import re

from django.conf import settings
from django.core.mail import send_mail
from celery.decorators import task


def check_for_manager_access(user_data):
    user_data['is_manager'] = False
    email = user_data.get('email')
    if email:
        for domain_pattern in settings.ALLOWED_MANAGER_EMAIL_DOMAINS:
            valid_manager_email = re.match(r'.+@' + domain_pattern + '$', email)
            if valid_manager_email:
                user_data['is_manager'] = True
                break

    return user_data


@task(name="send_email")
def send_reminder_email(data):
    send_mail(
        'Update - repair request',
        f'Your device {data["manufacturer"]} {data["model"]}'\
            + f' {data["serial_number"]}'\
            + ' has been serviced. Thank you for your patience.',
        'repairyourstuff123@gmail.com',
        [data["email"]],
        fail_silently=False
    )
