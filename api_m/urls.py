from django.urls import path, re_path

from . import views


app_name = 'api-m'
urlpatterns = [
    path('register/', views.register_api, name='register-user'),
    path('login/', views.login_api, name='login-user'),
    path('logout/', views.logout_api, name='logout-user'),
    path('update-password/', views.update_password, name='update-password'),

    path('types/', views.TypeListManager.as_view(), name='device-list'),

    path('devices/', views.DeviceListManager.as_view(), name='device-list'),
    path('devices/<int:pk>', views.DeviceDetailManager.as_view(),
         name='device-detail'),

    path('requests/<int:pk>', views.RequestDetailManager.as_view(),
         name='request-detail'),
    re_path('requests/(?P<term>.*)', views.RequestListManager.as_view(),
            name='request-list'),

    path('actions/', views.ActionListManager.as_view(), name='action-list'),

    path('stages/', views.StageListManager.as_view(), name='stage-list'),

    path('current_user/', views.current_user, name='current-user'),

]
