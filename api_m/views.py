"""Contains views for the service layer (API).
"""
from django.contrib.auth import authenticate
from django import db
from django.views.decorators import csrf
from rest_framework import generics, status, authtoken
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from . import models, serializers, utils
from . import permissions as custom_permissions


class DeviceListManager(generics.ListCreateAPIView):
    """Class which defines the view for the device list API."""
    queryset = models.Device.objects.all()
    serializer_class = serializers.DeviceSerializer
    permission_classes = [custom_permissions.ManagerOnly]


class RequestListManager(generics.ListCreateAPIView):
    '''Class which defines the view for the request list API.'''
    serializer_class = serializers.RequestSerializer
    permission_classes = [custom_permissions.ManagerOnly]

    def get_queryset(self):
        """Returns the required queryset based on the request URL."""
        last_stage_id = models.Stage.objects.last().id
        base_queryset = models.Request.objects
        queryset = {
            'completed': base_queryset.filter(stage_id=last_stage_id) \
                .order_by('created'),
            'pending': base_queryset.exclude(stage_id=last_stage_id) \
                .order_by('created'),
        }
        try:
            return queryset[self.kwargs['term']]
        except KeyError:
            return base_queryset.all().order_by('created')


class DeviceDetailManager(generics.RetrieveUpdateDestroyAPIView):
    '''Class which defines the view for the device detail API.'''
    queryset = models.Device.objects.all()
    serializer_class = serializers.DeviceSerializer
    permission_classes = [custom_permissions.ManagerOnly]


class RequestDetailManager(generics.RetrieveUpdateDestroyAPIView):
    '''Class which defines the view for the request detail API.'''
    queryset = models.Request.objects.all()
    serializer_class = serializers.RequestSerializer
    permission_classes = [custom_permissions.ManagerOnly]


class TypeListManager(generics.ListCreateAPIView):
    '''Class which defines the view for the device model list API.'''
    queryset = models.Type.objects.all()
    serializer_class = serializers.TypeSerializer
    permission_classes = [custom_permissions.ManagerOnly]


class ActionListManager(generics.ListCreateAPIView):
    '''Class which defines the view for the service action list API.'''
    queryset = models.Action.objects.all()
    serializer_class = serializers.ActionSerializer
    permission_classes = [custom_permissions.ManagerOnly]


class StageListManager(generics.ListCreateAPIView):
    '''Class which defines the view for the stage list API.'''
    queryset = models.Stage.objects.all()
    serializer_class = serializers.StageSerializer
    permission_classes = [custom_permissions.ManagerOnly]


@csrf.csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def register_api(request):
    """Handles POST requests to register a user."""
    user_data = request.data
    try:
        password = request.data.get("password")
        password2 = request.data.get("password2")
        if password != password2:
            return Response({'error': 'Passwords do not match.'},
                            status=status.HTTP_400_BAD_REQUEST)
        del user_data['password2']
    except KeyError:
        return Response({'error': 'Invalid input'},
                        status=status.HTTP_400_BAD_REQUEST)

    # Checks email to see if user is employee.
    user_data = utils.check_for_manager_access(user_data)

    serializer = serializers.UserSerializer(data=user_data)
    if serializer.is_valid():
        try:
            serializer.save()
        except db.utils.IntegrityError:
            return Response({"error": "Email is already in use."},
                            status=status.HTTP_409_CONFLICT)

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    return Response({"error": "Invalid input"},
                    status=status.HTTP_400_BAD_REQUEST)


@csrf.csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login_api(request):
    """Handles POST requests to login as a user."""
    email = request.data.get("email")
    password = request.data.get("password")
    if email is None or password is None:
        return Response({'error': 'Please provide both email and password'},
                        status=status.HTTP_400_BAD_REQUEST)
    user = authenticate(email=email, password=password)
    if not user:
        return Response({'error': 'Invalid Credentials'},
                        status=status.HTTP_401_UNAUTHORIZED)
    token, _ = authtoken.models.Token.objects.get_or_create(user=user)
    return Response({'token': token.key, 'is_manager': user.is_manager},
                    status=status.HTTP_200_OK)


@csrf.csrf_exempt
@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def logout_api(request):
    """Handles POST requests to log a user out."""
    request.user.auth_token.delete()
    return Response({"message": f"Logged out {request.user.email}."},
                    status=status.HTTP_200_OK)


@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def current_user(request):
    """Handles GET requests to get details of the currently logged in user."""
    serializer = serializers.UserSerializer(request.user)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def update_password(request):
    """Handles POST request to update password."""
    old_password = request.data.get("old_password", "")
    new_password = request.data.get("new_password", "")
    if not request.user.check_password(old_password):
        return Response({"message": "Invalid password"},
                        status=status.HTTP_401_UNAUTHORIZED)
    if len(new_password) < 8:
        return Response({"message": "Invalid password"},
                        status=status.HTTP_400_BAD_REQUEST)
    request.user.set_password(new_password)
    request.user.save()
    return Response({"message": "Password changed"}, status=status.HTTP_200_OK)
