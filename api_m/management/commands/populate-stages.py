import random
from django.core.management.base import BaseCommand
from api_m.models import Stage

STAGES = [
    'Registration',
    'Initial Inspection',
    'Troubleshooting',
    'Repair/Replacement',
    'Quality Checks',
    'Processed'
]


class Command(BaseCommand):
    help = 'Create stages'

    def _create_stages(self):
        for stage in STAGES:
            existing_stage = Stage.objects.filter(name=stage)
            if not existing_stage:
                Stage.objects.create(name=stage)

    def handle(self, *args, **options):
        self._create_stages()
