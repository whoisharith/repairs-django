import random
from django.core.management.base import BaseCommand
from api_m.models import Type

TYPES = [
    'Mobile Phone',
    'Television',
    'Laptop',
    'Gaming Console',
]


class Command(BaseCommand):
    help = 'Create types'

    def _create_types(self):
        for each_type in TYPES:
            existing_type = Type.objects.filter(name=each_type)
            if not existing_type:
                Type.objects.create(name=each_type)

    def handle(self, *args, **options):
        self._create_types()
