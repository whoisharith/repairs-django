"""Defines the admin page features for models in this app."""

from django.contrib import admin

from . import models


admin.site.register(models.User)
admin.site.register(models.Stage)
admin.site.register(models.Type)
admin.site.register(models.Device)
admin.site.register(models.Request)
admin.site.register(models.Action)
