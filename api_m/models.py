"""Contains ORM model definitions for the app."""

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone
from phonenumber_field import modelfields

from .managers import CustomUserManager


class User(AbstractUser):
    """Define a custom auth model with email as the username field."""
    is_manager = models.BooleanField(default=False)
    email = models.EmailField(unique=True)
    phone = modelfields.PhoneNumberField(null=False, blank=False)
    username = None
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email


class Stage(models.Model):
    """Stores details about each stage in the service process"""
    name = models.CharField(max_length=100, blank=False)

    def __str__(self):
        return self.name


class Type(models.Model):
    """Stores details of the types of devices serviced."""
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Device(models.Model):
    """Stores details about each device registered at the business."""
    serial_number = models.CharField(max_length=50)
    model = models.CharField(max_length=50)
    manufacturer = models.CharField(max_length=50)

    type_of_device = models.ForeignKey(Type, on_delete=models.CASCADE,
                                       related_name='devices')

    owner = models.ForeignKey(User, on_delete=models.CASCADE,
                              related_name='devices')

    def __str__(self):
        return f"{self.model} {self.serial_number}"


class Request(models.Model):
    '''Stores details about each request registered.'''
    description = models.CharField(max_length=1000)
    created = models.DateTimeField(default=timezone.now)

    device = models.ForeignKey(Device, on_delete=models.CASCADE,
                               related_name='requests')
    stage = models.ForeignKey(Stage, on_delete=models.CASCADE,
                              related_name='requests', default=1)

    def __str__(self):
        return f"{self.device.owner.email} {self.device.model}"\
            + f" {self.description[:30]}"


class Action(models.Model):
    """Stores details about actions on all registered devices."""
    stage_status = models.CharField(max_length=1000)
    timestamp = models.DateTimeField(default=timezone.now)

    handler = models.ForeignKey(User, on_delete=models.CASCADE,
                                related_name='actions')

    request = models.ForeignKey(Request, on_delete=models.CASCADE,
                                related_name='actions')

    to_stage = models.ForeignKey(Stage, on_delete=models.CASCADE,
                                 related_name='actions')

    def __str__(self):
        return f"{self.request.id} {self.to_stage.name}"
