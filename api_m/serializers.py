"""Serializers for the database models"""
from rest_framework import serializers

from drf_extra_fields.relations import PresentablePrimaryKeyRelatedField

from . import models, utils

class UserSerializer(serializers.ModelSerializer):
    """Serializes the Customer model."""
    requests = serializers.SerializerMethodField()
    email = serializers.EmailField(allow_blank=False)
    password = serializers.CharField(write_only=True)

    class Meta:
        """Meta class"""
        model = models.User
        fields = ['id', 'first_name', 'last_name', 'email', 'phone', 'requests',
                  'is_manager', 'password']
        read_only_fields = ['id', 'requests']

    def get_requests(self, user):
        devices = user.devices.all()
        all_requests = []
        for device in devices:
            for request in device.requests.all():
                data = {
                    'id': request.id,
                    'device': {
                        'id': device.id,
                        'manufacturer': device.manufacturer,
                        'model': device.model,
                        'serial_number': device.serial_number,
                        'type': device.type
                    },
                    'description': request.description,
                    'created': request.created,
                    'stage': request.stage.id,
                    "stage_status": request.actions.last().stage_status
                }
                all_requests.append(data)
        
        return all_requests

    def create(self, validated_data):
        data = validated_data
        email, password = data['email'], data['password']
        del data['email'], data['password']
        return models.User.objects.create_user(email, password, **data)


class TypeSerializer(serializers.ModelSerializer):
    """Serializes the Type model."""

    class Meta:
        """Meta class"""
        model = models.Type
        fields = ['id', 'name']


class DeviceSerializer(serializers.ModelSerializer):
    """Serializes the Device model."""
    type_of_device = PresentablePrimaryKeyRelatedField(
        queryset=models.Type.objects.all(),
        presentation_serializer=TypeSerializer
    )
    owner = serializers.SerializerMethodField()
    requests = serializers.SerializerMethodField()

    class Meta:
        """Meta class"""
        model = models.Device
        fields = ['id', 'manufacturer', 'model', 'serial_number',
                  'type_of_device', 'owner', 'requests']

    def get_owner(self, device):
        return {
            "id": device.owner.id,
            "email": device.owner.email,
        }

    def get_requests(self, device):
        requests = []
        for request in device.requests.all():
            data = {
                "id": request.id,
                "description": request.description,
                "stage": {"id": request.stage.id, "name": request.stage.name},
                "stage_status": request.actions.last().stage_status
            }
            requests.append(data)
        return requests

    def create(self, validated_data, *args, **kwargs):
        print(self.context)
        validated_data.update({"owner": (self.context['request']).user})
        return models.Device.objects.create(**validated_data)


class RequestSerializer(serializers.ModelSerializer):
    """Serializes the Request model."""
    device = PresentablePrimaryKeyRelatedField(
        queryset=models.Device.objects.all(),
        presentation_serializer=DeviceSerializer
    )
    actions = serializers.SerializerMethodField()
    created = serializers.SerializerMethodField()
    stage = serializers.SerializerMethodField()

    class Meta:
        """Meta class"""
        model = models.Request
        fields = ['id', 'description', 'device', 'created', 'stage', 'actions']
        read_only_fields = ['id', 'created', 'stage', 'actions']

    def create(self, validated_data, *args, **kwargs):
        request = models.Request.objects.create(**validated_data)
        request.actions.create(
            stage_status="Registration complete.",
            handler_id=self.context["request"].user.id,
            to_stage_id=1
        )
        return request

    def get_stage(self, request):
        return {
            'id': request.stage.id,
            'name': request.stage.name
        }

    def get_actions(self, request):
        actions = []
        for action in request.actions.all():
            data = {
                'id': action.id,
                'stage_status': action.stage_status,
                'handler': action.handler.id,
                'to_stage': action.to_stage.name,
                'timestamp': action.timestamp.strftime("%Y-%m-%d %H:%M:%S %Z")
            }
            actions.append(data)
        return actions

    def get_created(self, request):
        return request.created.strftime("%Y-%m-%d %H:%M:%S %Z")


class StageSerializer(serializers.ModelSerializer):
    """Serializes the Stage model."""

    class Meta:
        """Meta class"""
        model = models.Stage
        fields = ['id', 'name']


class ActionSerializer(serializers.ModelSerializer):
    """Serializes the Action model."""
    request = serializers.PrimaryKeyRelatedField(
        queryset=models.Request.objects.all())

    to_stage = serializers.PrimaryKeyRelatedField(
        queryset=models.Stage.objects.all()
    )

    class Meta:
        """Meta class"""
        model = models.Action
        fields = ['id', 'handler', 'request', 'to_stage',
                  'timestamp', 'stage_status']
        read_only_fields = ['timestamp', 'id']

    def create(self, validated_data, *args, **kwargs):
        """Overrides the default create method of ModelSerializer to include
        the handler id while saving an action. Also sends an email if a request
        reaches the last stage."""
        validated_data.update({"handler": (self.context['request']).user})
        request = models.Request.objects.get(pk=validated_data['request'].id)
        request.stage_id = validated_data['to_stage'].id
        request.save()
        if validated_data['to_stage'] == models.Stage.objects.last():
            email_data = {
                'manufacturer': request.device.manufacturer,
                'model': request.device.model,
                'serial_number': request.device.serial_number,
                'email': request.device.owner.email
            }
            utils.send_reminder_email.delay(email_data)
        return models.Action.objects.create(**validated_data)

    def validate_to_stage(self, to_stage):
        """Validates the stage input while moving updating a request status."""
        request = models.Request.objects.get(pk=self.initial_data['request'])
        if to_stage.id not in [request.stage.id, request.stage.id + 1]\
            or to_stage.id > models.Stage.objects.last().id:

            raise serializers.ValidationError("Invalid destination stage")

        return to_stage
